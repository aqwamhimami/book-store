package com.project.bookstore.repository;

import com.project.bookstore.model.Authors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository  extends JpaRepository<Authors, Long>{
    
} 