package com.project.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import com.project.bookstore.model.Books;
import com.project.bookstore.repository.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/books")
public class BookController{
    
    @Autowired
    BookRepository bookRepository;

    // GET ALL
    @GetMapping("/getAll")
    public List<Books> getAllBooks() {
        return bookRepository.findAll();
    } 

    // ADD NEW
    @PostMapping("/create")
    public Books createBook(@Valid @RequestBody Books book) {
        return bookRepository.save(book);
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Books getOneBookById(@PathVariable(value = "id") Long bookId){
        return bookRepository.findById(bookId).get();
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Books updateBook(@PathVariable(value = "id") Long bookId,
                                            @Valid @RequestBody Books bookDetails) {

        Books book = bookRepository.findById(bookId).get();

        book.setTitle(bookDetails.getTitle());
        book.setDescription(bookDetails.getDescription());
        book.setReleaseDate(bookDetails.getReleaseDate());
        book.setAuthors(bookDetails.getAuthors());
        book.setPublisher(bookDetails.getPublisher());
        book.setCategories(bookDetails.getCategories());

        Books updatedBook = bookRepository.save(book);
        return updatedBook;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long bookId) {
        Books book = bookRepository.findById(bookId).get();

        bookRepository.delete(book);

        return ResponseEntity.ok().build();
    }
}