package com.project.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import com.project.bookstore.model.Categories;
import com.project.bookstore.repository.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
public class CategoryController{
    
    @Autowired
    CategoryRepository categoryRepository;

    // GET ALL
    @GetMapping("/getAll")
    public List<Categories> getAllCategory() {
        return categoryRepository.findAll();
    } 

    // ADD NEW
    @PostMapping("/create")
    public Categories createCategory(@Valid @RequestBody Categories category) {
        return categoryRepository.save(category);
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Categories getOneCategoryById(@PathVariable(value = "id") Long categoryId){
        return categoryRepository.findById(categoryId).get();
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Categories updateCategory(@PathVariable(value = "id") Long categoryId,
                                            @Valid @RequestBody Categories categoryDetails) {

        Categories category = categoryRepository.findById(categoryId).get();
        category.setCategoryName(categoryDetails.getCategoryName());
        category.setCategoryDescription(categoryDetails.getCategoryDescription());

        Categories updateCategory = categoryRepository.save(category);
        return updateCategory;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable(value = "id") Long categoryId) {
        Categories category = categoryRepository.findById(categoryId).get();

        categoryRepository.delete(category);

        return ResponseEntity.ok().build();
    }
}