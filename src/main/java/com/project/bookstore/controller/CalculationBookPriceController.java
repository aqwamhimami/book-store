package com.project.bookstore.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.project.bookstore.model.Books;
import com.project.bookstore.repository.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/books/price")
public class CalculationBookPriceController{
    
    @Autowired
    BookRepository bookRepository;

    
    // UPDATE
    @PutMapping("/update")
    public HashMap<String, Object> updateBookPrice() {
        List<Books> book = bookRepository.findAll();
        book = calculationPrice(book);

        for (Books bookDetail : book) {
            bookRepository.save(bookDetail);
        }

        HashMap<String, Object> messageSuccess = new HashMap<String, Object>();
        messageSuccess.put(".Status", 200);
        messageSuccess.put("Message", "Book price calculation is success");
        return messageSuccess;
    }

    public List<Books> calculationPrice(List<Books> book){
        book = calculationBasicPrice(book);
        book = calculationTax(book);

        return book;
    }

    public List<Books> calculationBasicPrice(List<Books> book) {
        for (Books bookDetail : book) {
            double basicPrice, baseProduction, ratePrice;
            Date releaseDate = bookDetail.getReleaseDate();
            int releaseYear = releaseDate.getYear();
            Date currentDate = Calendar.getInstance().getTime();
            int currentYear = currentDate.getYear();

            if (releaseYear == currentYear) {
                ratePrice = 1.5;
            } else {
                ratePrice = 1.3;
            }

            baseProduction = bookDetail.getPublisher().getGrade().getBaseProduction();
            basicPrice = ratePrice * baseProduction;

            bookDetail.setPrice(basicPrice);
        }

        return book;
    }

    public List<Books> calculationTax(List<Books> book){
        for (Books bookDetail : book) {
            String bookReleaseCountry = bookDetail.getPublisher().getCountry(), 
                    ina = "Indonesia";
            double finalPrice, taxRate;
            if (bookReleaseCountry.equalsIgnoreCase(ina)) {
                taxRate = 0.05;
            } else {
                taxRate = 0.1;
            }

            finalPrice = bookDetail.getPrice() + (bookDetail.getPrice() * taxRate);
            bookDetail.setPrice(finalPrice);
        }
        return book;
    }
    
}