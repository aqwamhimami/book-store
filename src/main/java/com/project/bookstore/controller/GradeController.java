package com.project.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import com.project.bookstore.model.Grade;
import com.project.bookstore.repository.GradeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/grades")
public class GradeController{
    
    @Autowired
    GradeRepository gradeRepository ;

    // GET ALL
    @GetMapping("/getAll")
    public List<Grade> getAllGrade() {
        return gradeRepository.findAll();
    } 

    // ADD NEW
    @PostMapping("/create")
    public Grade createGrade(@Valid @RequestBody Grade grade) {
        return gradeRepository.save(grade);
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Grade getOneGradeById(@PathVariable(value = "id") Long gradeId){
        return gradeRepository.findById(gradeId).get();
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Grade updatePublisher(@PathVariable(value = "id") Long gradeId,
                                            @Valid @RequestBody Grade gradeDetail) {

        Grade grade = gradeRepository.findById(gradeId).get();
        grade.setQuality(gradeDetail.getQuality());
        grade.setBaseProduction(gradeDetail.getBaseProduction());

        Grade updateGrade = gradeRepository.save(grade);
        return updateGrade;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteGrade(@PathVariable(value = "id") Long gradeId) {
        Grade grade = gradeRepository.findById(gradeId).get();

        gradeRepository.delete(grade);

        return ResponseEntity.ok().build();
    }
}