package com.project.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import com.project.bookstore.model.Authors;
import com.project.bookstore.repository.AuthorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/authors")
public class AuthorController{
    
    @Autowired
    AuthorRepository authorRepository;

    // GET ALL
    @GetMapping("/getAll")
    public List<Authors> getAllAuthors() {
        return authorRepository.findAll();
    } 

    // ADD NEW
    @PostMapping("/create")
    public Authors createBook(@Valid @RequestBody Authors author) {
        return authorRepository.save(author);
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Authors getOneAuthorById(@PathVariable(value = "id") Long authorId){
        return authorRepository.findById(authorId).get();
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Authors updateNote(@PathVariable(value = "id") Long authorId,
                                            @Valid @RequestBody Authors authorDetails) {

        Authors author = authorRepository.findById(authorId).get();

        author.setFirstName(authorDetails.getFirstName());
        author.setLastName(authorDetails.getLastName());

        Authors updateAuthor = authorRepository.save(author);
        return updateAuthor;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable(value = "id") Long authorId) {
        Authors author = authorRepository.findById(authorId).get();

        authorRepository.delete(author);

        return ResponseEntity.ok().build();
    }
}