package com.project.bookstore.modelDTO;

/**
 * GradeDTO
 */
public class GradeDTO {
    private long id;
	private Double baseProduction;
    private String quality;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Double getBaseProduction() {
		return baseProduction;
	}
	public void setBaseProduction(Double baseProduction) {
		this.baseProduction = baseProduction;
	}
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
    
}