package com.project.bookstore.modelDTO;

import java.util.Date;

public class BookDTO {
    private Long id;
	private AuthorDTO authors;
	private CategoriesDTO categories;
	private PublisherDTO publisher;
	private String description;
	private Double price;
	private Date releaseDate;
    private String title;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public AuthorDTO getAuthors() {
		return authors;
	}
	public void setAuthors(AuthorDTO authors) {
		this.authors = authors;
	}
	public CategoriesDTO getCategories() {
		return categories;
	}
	public void setCategories(CategoriesDTO categories) {
		this.categories = categories;
	}
	public PublisherDTO getPublisher() {
		return publisher;
	}
	public void setPublisher(PublisherDTO publisher) {
		this.publisher = publisher;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
    }
    
}
    
    