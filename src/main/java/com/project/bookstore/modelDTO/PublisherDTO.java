package com.project.bookstore.modelDTO;

public class PublisherDTO {

    private long id;
	private GradeDTO grade;
	private String country;
    private String publisherName;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public GradeDTO getGrade() {
		return grade;
	}
	public void setGrade(GradeDTO grade) {
		this.grade = grade;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

}