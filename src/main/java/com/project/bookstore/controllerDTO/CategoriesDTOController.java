package com.project.bookstore.controllerDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.bookstore.model.Categories;
import com.project.bookstore.modelDTO.CategoriesDTO;
import com.project.bookstore.repository.CategoryRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categoryDTO")
public class CategoriesDTOController{
    
    @Autowired
    CategoryRepository categoryRepository;

    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/getAll")
    public Map<String, Object> getAllCategory() {
        Map<String,Object> result = new HashMap<String,Object>();
        List<Categories> listCategoriesEntity = categoryRepository.findAll();
        List<CategoriesDTO> listCategoriesDTO = new ArrayList<CategoriesDTO>();

        for (Categories category : listCategoriesEntity) {
            CategoriesDTO categoryDTO = new CategoriesDTO();

            categoryDTO =  modelMapper.map(category, CategoriesDTO.class);

            listCategoriesDTO.add(categoryDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listCategoriesDTO);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createCategoryDTO(@Valid @RequestBody CategoriesDTO categoryDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Categories categoryEntity = new Categories();
        categoryEntity = modelMapper.map(categoryDTO, Categories.class);
        
        categoryRepository.save(categoryEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", categoryEntity);

        return result; 
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneCategoryById(@PathVariable(value = "id") Long categoryId){
        Map<String, Object> result = new HashMap<String, Object>();
        Categories categoryEntity = categoryRepository.findById(categoryId).get();
        CategoriesDTO categoryDTO  = new CategoriesDTO();
        
        categoryDTO = modelMapper.map(categoryEntity, CategoriesDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", categoryDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateCategory(@PathVariable(value = "id") Long categoryId,
                                            @Valid @RequestBody CategoriesDTO categoryDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Categories categoryEntity = categoryRepository.findById(categoryId).get();
        
        categoryEntity = modelMapper.map(categoryDTO, Categories.class);
        categoryEntity.setId(categoryId);

        categoryRepository.save(categoryEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", categoryDTO);

        return result;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public Map<String, Object> deleteAuthor(@PathVariable(value = "id") Long categoryId) {
        Map<String, Object> result = new HashMap<String, Object>();
        Categories category = categoryRepository.findById(categoryId).get();

        categoryRepository.delete(category);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }
}