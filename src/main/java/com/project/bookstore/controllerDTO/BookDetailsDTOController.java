package com.project.bookstore.controllerDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// import javax.validation.Valid;

import com.project.bookstore.repository.BookDetailsRepository;
import com.project.bookstore.viewDTOModels.BookDetailsDTO;
import com.project.bookstore.viewModels.BookDetails;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/bookDetails")
public class BookDetailsDTOController{
    
    @Autowired
    BookDetailsRepository bookDetailsRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<BookDetails> listBookDetailsEntity = bookDetailsRepository.findAll();
        List<BookDetailsDTO> listBookDetailsDTO = new ArrayList<BookDetailsDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (BookDetails bookDetailsEntity : listBookDetailsEntity) {
            BookDetailsDTO bookDetailsDTO = new BookDetailsDTO();
            bookDetailsDTO = modelMapper.map(bookDetailsEntity, BookDetailsDTO.class);

            listBookDetailsDTO.add(bookDetailsDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listBookDetailsDTO);

        return result;
    } 
    
    // GET ONE
    @GetMapping("/getOne/{oo}")
    public Map<String,Object> getOneBookDetailsById(@PathVariable(value = "oo") Long bookDetailId){
        Map<String, Object> result = new HashMap<String, Object>();
        List<BookDetails> bookDetailsEntity = bookDetailsRepository.findAll();
        BookDetailsDTO bookDetailsDTO  = new BookDetailsDTO();

        for (BookDetails bookDetails : bookDetailsEntity) {
            if (bookDetails.getId() == bookDetailId) {
                bookDetailsDTO = modelMapper.map(bookDetails, BookDetailsDTO.class);
            }
        }

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", bookDetailsDTO);
        
        return result;
    }

}