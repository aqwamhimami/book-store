package com.project.bookstore.controllerDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.bookstore.model.Books;
import com.project.bookstore.modelDTO.BookDTO;
import com.project.bookstore.repository.BookRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/bookDTO")
public class BookDTOController {

    @Autowired
    BookRepository bookRepository;
    ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/getAll")
    public Map<String, Object> getAllBookDTO() {
        Map<String, Object> result = new HashMap<String, Object>();
        List<Books> listBookEntity = bookRepository.findAll();
        List<BookDTO> listBookDTO = new ArrayList<BookDTO>();

        for (Books book : listBookEntity) {
            BookDTO bookDTO = new BookDTO();
            bookDTO = modelMapper.map(book, BookDTO.class);

            listBookDTO.add(bookDTO);
        }
        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listBookDTO);
        
        return result;
    }

    @PostMapping("/create")
    public Map<String, Object> createBookDTO(@Valid @RequestBody BookDTO bookDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Books bookEntity = new Books();
        bookEntity = modelMapper.map(bookDTO, Books.class);
        
        bookRepository.save(bookEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", bookDTO);

        return result; 
    }

    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneBookById(@PathVariable(value = "id") Long bookId){
        Map<String, Object> result = new HashMap<String, Object>();
        Books bookEntity = bookRepository.findById(bookId).get();
        BookDTO bookDTO  = new BookDTO();
        
        bookDTO = modelMapper.map(bookEntity, BookDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", bookDTO);
        
        return result;
    }

    @PutMapping("/update/{id}")
    public Map<String, Object> updateBook(@PathVariable(value = "id") Long bookId,
                                            @Valid @RequestBody BookDTO bookDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Books bookEntity = bookRepository.findById(bookId).get();
        
        bookEntity = modelMapper.map(bookDTO, Books.class);
        bookEntity.setId(bookDTO.getId());

        bookRepository.save(bookEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", bookDTO);

        return result;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public Map<String, Object> deleteBook(@PathVariable(value = "id") Long bookId) {
        Map<String, Object> result = new HashMap<String, Object>();
        Books book = bookRepository.findById(bookId).get();

        bookRepository.delete(book);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}