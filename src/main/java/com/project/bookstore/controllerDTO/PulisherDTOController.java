package com.project.bookstore.controllerDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.bookstore.model.Publisher;
import com.project.bookstore.modelDTO.PublisherDTO;
import com.project.bookstore.repository.PublisherRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/publisherDTO")
public class PulisherDTOController {

    @Autowired
    PublisherRepository publisherRepository;
    ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/getAll")
    public Map<String, Object> getAllPulisherDTO() {
        Map<String, Object> result = new HashMap<String, Object>();
        List<Publisher> listPublisherEntity = publisherRepository.findAll();
        List<PublisherDTO> listPublisherDTO = new ArrayList<PublisherDTO>();

        for (Publisher publisher : listPublisherEntity) {
            PublisherDTO publisherDTO = new PublisherDTO();
            publisherDTO = modelMapper.map(publisher, PublisherDTO.class);

            listPublisherDTO.add(publisherDTO);
        }
        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listPublisherDTO);
        
        return result;
    }

    @PostMapping("/create")
    public Map<String, Object> createPublisherDTO(@Valid @RequestBody PublisherDTO publisherDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Publisher publisherEntity = new Publisher();
        publisherEntity = modelMapper.map(publisherDTO, Publisher.class);
        
        publisherRepository.save(publisherEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", publisherDTO);

        return result; 
    }

    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneGraderById(@PathVariable(value = "id") Long publisherID){
        Map<String, Object> result = new HashMap<String, Object>();
        Publisher publisherEntity = publisherRepository.findById(publisherID).get();
        PublisherDTO publisherDTO  = new PublisherDTO();
        
        publisherDTO = modelMapper.map(publisherEntity, PublisherDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", publisherDTO);
        
        return result;
    }

    @PutMapping("/update/{id}")
    public Map<String, Object> updatePulisher(@PathVariable(value = "id") Long publisherId,
                                            @Valid @RequestBody PublisherDTO publisherDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Publisher publisherEntity = publisherRepository.findById(publisherId).get();
        
        publisherEntity = modelMapper.map(publisherDTO, Publisher.class);
        publisherEntity.setId(publisherId);

        publisherRepository.save(publisherEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", publisherDTO);

        return result;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public Map<String, Object> deletePubliser(@PathVariable(value = "id") Long publisherID) {
        Map<String, Object> result = new HashMap<String, Object>();
        Publisher publisher = publisherRepository.findById(publisherID).get();

        publisherRepository.delete(publisher);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}