package com.project.bookstore.controllerDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.bookstore.model.Authors;
import com.project.bookstore.modelDTO.AuthorDTO;
import com.project.bookstore.repository.AuthorRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/authorDTO")
public class AuthorDTOController{
    
    @Autowired
    AuthorRepository authorRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Authors> listAuthorsEntity = authorRepository.findAll();
        List<AuthorDTO> listDTOAuthor = new ArrayList<AuthorDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Authors authorEntity : listAuthorsEntity) {
            AuthorDTO authorDTO = new AuthorDTO();
            authorDTO = modelMapper.map(authorEntity, AuthorDTO.class);

            listDTOAuthor.add(authorDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOAuthor);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody AuthorDTO authorDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Authors authorEntity = new Authors();

        authorEntity = modelMapper.map(authorDTO, Authors.class);
        authorRepository.save(authorEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", authorEntity);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneAuthorById(@PathVariable(value = "id") Long authorId){
        Map<String, Object> result = new HashMap<String, Object>();
        Authors authorEntity = authorRepository.findById(authorId).get();
        AuthorDTO authorDTO  = new AuthorDTO();
        
        authorDTO = modelMapper.map(authorEntity, AuthorDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", authorDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateAuthor(@PathVariable(value = "id") Long authorId,
                                            @Valid @RequestBody AuthorDTO authorDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Authors authorEntity = authorRepository.findById(authorId).get();
        
        authorEntity.setFirstName(authorDTO.getFirstName());
        authorEntity.setLastName(authorDTO.getLastName());

        authorRepository.save(authorEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", authorDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long authorId) {
        Authors author = authorRepository.findById(authorId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        authorRepository.delete(author);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }
}

    

       