package com.project.bookstore.controllerDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.bookstore.model.Grade;
import com.project.bookstore.modelDTO.GradeDTO;
import com.project.bookstore.repository.GradeRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/gradeDTO")
public class GradeDTOController{
    
    @Autowired
    GradeRepository gradeRepository ;

    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/getAll")
    public Map<String, Object> getAllGrade() {
        Map<String,Object> result = new HashMap<String,Object>();
        List<Grade> listGradeEntity = gradeRepository.findAll();
        List<GradeDTO> listGradeDTO = new ArrayList<GradeDTO>();

        for (Grade grade : listGradeEntity) {
            GradeDTO gradeDTO = new GradeDTO();

            gradeDTO =  modelMapper.map(grade, GradeDTO.class);

            listGradeDTO.add(gradeDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listGradeDTO);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createGradeDTO(@Valid @RequestBody GradeDTO gradeDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Grade gradeEntity = new Grade();
        gradeEntity = modelMapper.map(gradeDTO, Grade.class);
        
        gradeRepository.save(gradeEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", gradeDTO);

        return result; 
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneGraderById(@PathVariable(value = "id") Long gradeID){
        Map<String, Object> result = new HashMap<String, Object>();
        Grade gradeEntity = gradeRepository.findById(gradeID).get();
        GradeDTO gradeDTO  = new GradeDTO();
        
        gradeDTO = modelMapper.map(gradeEntity, GradeDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", gradeDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateGrade(@PathVariable(value = "id") Long gradeID,
                                            @Valid @RequestBody GradeDTO gradeDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Grade gradeEntity = gradeRepository.findById(gradeID).get();
        
        gradeEntity = modelMapper.map(gradeDTO, Grade.class);
        gradeEntity.setId(gradeID);

        gradeRepository.save(gradeEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", gradeDTO);

        return result;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public Map<String, Object> deleteAuthor(@PathVariable(value = "id") Long gradeId) {
        Map<String, Object> result = new HashMap<String, Object>();
        Grade grade = gradeRepository.findById(gradeId).get();

        gradeRepository.delete(grade);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }
}