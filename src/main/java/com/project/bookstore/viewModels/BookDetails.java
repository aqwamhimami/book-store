package com.project.bookstore.viewModels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "list_book", schema = "public")
public class BookDetails {

    private int no;
    private long id;
	private String description;
	private Double price;
	private Date releaseDate;
    private String title;
	private String firstName;
	private String publisherName;
	private String categoryName;
    private String quality;

    @Id

    @Column(name = "no", unique = true, nullable = false)
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
    }
    
    @Column(name = "id")
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
    }
    
    @Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
    }
    
    @Column(name = "price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name = "releaseDate")
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
    }
    
    @Column(name = "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
    }
    
    @Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
    }
    
    @Column(name = "publisherName")
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
    }
    
    @Column(name = "categoryName")
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
    }
    
    @Column(name ="quality")
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
    
}